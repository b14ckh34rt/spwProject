from django.contrib.auth.models import Group
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType


from .models import *

try:
    userselection_model = ContentType.objects.get_for_model(UserSelection)
    destination_model = ContentType.objects.get_for_model(Destination)
    user_model = ContentType.objects.get_for_model(User)

    userselection_model_permissions = Permission.objects.filter(content_type=userselection_model)
    destination_model_permissions = Permission.objects.filter(content_type=destination_model)
    user_model_permissions = Permission.objects.filter(content_type=user_model)

    clientuser_group, clientuser_created = Group.objects.get_or_create(name='Clientuser')
    clientuser_group.permissions.set(userselection_model_permissions)

    moderator_group, moderator_created = Group.objects.get_or_create(name='Moderator')
    moderator_permissions = list()
    moderator_permissions.extend(destination_model_permissions)
    moderator_permissions.extend(user_model_permissions)
    moderator_group.permissions.set(moderator_permissions)
except:
    pass

def is_clientuser(user):
    return user.groups.filter(name='Clientuser').exists()
def is_moderator(user):
    return user.groups.filter(name='Moderator').exists()
